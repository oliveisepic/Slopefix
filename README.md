# Slopefix

A very lightweight and simple Bunny Hop slopefix plugin for Garry's Mod.

# Description

Enables users to properly Bunny Hop on angled surfaces and give the proper velocity amount. Fixes problems with the source engine not applying any velocity to the user.
This plugin was specifically made to run on Flow Network Bunny Hop gamemodes, but it should also work on any Sandbox derived gamemode.

# Installation

Everything you need will be in one file, slopefix.lua. Installation is very simple.

You can either place this file in "garrysmod\lua\autorun\server\slopefix.lua" or you can include it in your gamemode.

Configuration options are also available. Includes a debugger.

# Credits

This plugin was directly ported from Blacky's Slope Boost fix module, you can find that here: https://forums.alliedmods.net/showthread.php?p=2322788
Math adjusted by me, but mostly provided by Mev and Blacky.